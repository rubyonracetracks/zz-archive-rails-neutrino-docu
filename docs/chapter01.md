---
id: chapter01
title: Chapter 1: Prerequisites
sidebar_label: Chapter 1: Prerequisites
---

* Docker should be installed.  This is covered in the [Different Docker Tutorial](https://github.com/rubyonracetracks/tutorial-docker-stretch).
* You should have one of the rails-general Docker images installed.  This is covered in the [Different Docker Tutorial](https://github.com/rubyonracetracks/tutorial-docker-stretch).
* You should be familiar with the process of creating a new Rails app.
